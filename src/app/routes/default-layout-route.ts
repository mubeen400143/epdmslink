import {Routes} from '@angular/router';

export const DEFAULT_ROUTES:Routes=[
{
     path: '',
     loadChildren: () => import('../_LayoutDefault/login/login.module').then(m => m.LoginModule) 
}] 