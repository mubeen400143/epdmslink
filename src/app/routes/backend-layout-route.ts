import { Routes } from '@angular/router';


export const BACKEND_ROUTES: Routes = [

     {
          path: 'Dashboard',
          loadChildren: () => import('../_LayoutDashboard/dashboard/dashboard.module').then(m => m.DashboardModule)
     },

     {
          path: 'Portfolio',
          loadChildren: () => import('../_LayoutDashboard/home/home.module').then(m => m.HomeModule)
     },

     {
          path: 'CreatePortfolio',
          loadChildren: () => import('../_LayoutDashboard/projects-add/projects-add.module').then(m => m.ProjectsAddModule)
     },

     {
          path: 'ProjectsSummary',
          loadChildren: () => import('../_LayoutDashboard/projects-summary/projects-summary.module').then(m => m.ProjectsSummaryModule)
     },

     {
          path: 'PerformanceDashboard',
          loadChildren: () => import('../_LayoutDashboard/projects-chart/projects-chart.module').then(m => m.ProjectsChartModule)
     },

     {
          path: 'ViewProjects/:Mode',
          pathMatch: 'full', loadChildren: () => import('../_LayoutDashboard/view-dashboard-projects/view-dashboard-projects.module').then(m => m.ViewDashboardProjectsModule)
     },

     {
          path: 'DeleteHistory',
          loadChildren: () => import('../_LayoutDashboard/history/history.module').then(m => m.HistoryModule)
     },

]


