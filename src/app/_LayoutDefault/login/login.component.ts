import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginDTO } from 'src/app/_Models/login-dto';
import { UserDetailsDTO } from 'src/app/_Models/user-details-dto';
import { NotificationService } from 'src/app/_Services/notification.service';
import { Login } from 'src/app/_Interface/login';
import { AuthService } from 'src/app/_Services/auth.service'
import { ProjectTypeService } from 'src/app/_Services/project-type.service';
import { ChangeDetectorRef } from '@angular/core';
import { LoadingBarService } from '@ngx-loading-bar/core';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  _LoginForm: FormGroup;
  submitted = false;
  Obj_ILoginDTO: Login;
  DB_username: string;
  DB_password: string;
  UserDetails_List: UserDetailsDTO[];
  isLoading = false;
  InValidPassword = false;
  InValidUserName = false;

  //private ObjHomeComp: HomeComponent
  EmpNo: string; EmpCompNo: string; SystemRole: string;
  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private notifyService: NotificationService,
    private cd: ChangeDetectorRef,
    private authService: AuthService,
    private service: ProjectTypeService,
    private loadingBar: LoadingBarService) {
    this.Obj_ILoginDTO = new LoginDTO;
    this.loginForm = this.formBuilder.group({
      userid: ['', Validators.required],
      password: ['', Validators.required]
    });
  }
  //Login Form Variables
  // model: Login = { UserName: '', Password: '' }

  loginForm: FormGroup;
  message: string;
  returnUrl: string;
  User_FullName: string;
  //---end---
  ngOnInit() {
    this.returnUrl = 'backend/Dashboard';
    this.authService.logout();
  }
  get f() { return this.loginForm.controls; }

  OnKeyPress() {
    this.InValidPassword = false;
    this.InValidUserName = false;
    this.message = '';
  }

  cosnt_Loadingbar = this.loadingBar.useRef('http');
  login() {

    //debugger
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    else {
      // this.Sendlogin_Credentials();
      // debugger
      this.Obj_ILoginDTO.UserName = this.f.userid.value;
      this.Obj_ILoginDTO.Password = this.f.password.value;
      //alert("One");
      this.cosnt_Loadingbar.start();
      this.service.LoginCredentials(this.Obj_ILoginDTO)
        .subscribe(
          (data) => {
            // alert("LoginCredentials");
            debugger
            this.UserDetails_List = data as UserDetailsDTO[];
            this.message = this.UserDetails_List[0]['Message'];
            this.cosnt_Loadingbar.stop();
            this.DB_username = this.UserDetails_List[0]['UserName'].replace(/\s/g, "").toLowerCase();
            this.DB_password = this.UserDetails_List[0]['Password'].replace(/\s/g, "");
            this.User_FullName = this.UserDetails_List[0]['TM_DisplayName']

            if (this.f.userid.value.toLowerCase() == this.DB_username && this.f.password.value == this.DB_password) {
              console.log("Login successful");

              // alert("successful Login");
              localStorage.setItem('isLoggedIn', "true");
              this.InValidPassword = false;
              this.cd.detectChanges();
              this.EmpNo = data[0]['Emp_No'].replace(/\s/g, "");
              this.EmpCompNo = data[0]['Emp_Comp_No'].replace(/\s/g, "");
              this.SystemRole = data[0]['Emp_SystemRole'];
              sessionStorage.setItem('EmpNo', this.EmpNo);
              sessionStorage.setItem('EmpCompNo', this.EmpCompNo);
              sessionStorage.setItem('SystemRole', this.SystemRole);
              localStorage.setItem("UserfullName", this.User_FullName);
              localStorage.setItem('_Currentuser', this.DB_username);

              this.router.navigate([this.returnUrl]);

              // alert(this.returnUrl);
              this.notifyService.showInfo(this.User_FullName + ' ' + ' ', 'Login By :');
              this.notifyService.showSuccess("Successfully", "Logged In");
            }
            else {
              this.InValidPassword = true;
              console.log("Invalid Login");
              this.authService.logout();
              this.cd.detectChanges();
              // alert("Invalid");
              // this.message = "Please check your UserName and Password";
            }
          });
    }
  }
}


//Testing Code
// AddArray() {
//   let array1: any = [{ MailId: 1 },{ MailId: 2 },{ MailId: 3 }];
//   let array2: any = [{ MailId: 4 }, { MailId: 5 }, { MailId: 6 }]

//     let addAll : any=[];
   
//     console.log("Final Result--->",JSON.stringify(array2.concat(array1)));
// }