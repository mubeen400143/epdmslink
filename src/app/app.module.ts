import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {  FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { AppComponent } from './app.component';
import { SidebarComponent } from './_LayoutDashboard/sidebar/sidebar.component';
import { MaterialModule } from "src/app/material-module";
import { RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { ToastrModule } from 'ngx-toastr';
import { BackendLayoutComponent } from './_Layouts/backend-layout/backend-layout.component';
//import { SortDirective } from 'src/app/Directive/sort.directive';
import { ConfirmDialogComponent } from 'src/app/Shared/components/confirm-dialog/confirm-dialog.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
//import { DateAgoPipe } from './Shared/DateAgo/date-ago.pipe';
//import { DateFormatPipePipe } from 'src/app/Shared/date-format-pipe.pipe';
//import {AutocompleteLibModule} from 'angular-ng-autocomplete';

@NgModule({
  declarations: [
    AppComponent,
    BackendLayoutComponent,
    //DefaultLayoutComponent,
   SidebarComponent,
    ConfirmDialogComponent,
    //DateAgoPipe
    //,DateFormatPipePipe
   // SortDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MaterialModule,
    HttpClientModule,
    ToastrModule.forRoot({}),
    RouterModule,
    LoadingBarRouterModule,
    FormsModule,
    NgbModule,
    //AutocompleteLibModulel
  ],
  providers: [AuthGuard],
 
  bootstrap: [AppComponent]
})
export class AppModule { }
