import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/_Services/auth.service';
import { LoadingBarService } from '@ngx-loading-bar/core';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  _CurrentUser: string;
  _fullname: string;
  _EmpNo: string;
  constructor(private router: Router,
    private authService: AuthService,
    private loadingBar: LoadingBarService) { }

  loadingBar_state = this.loadingBar.useRef('http');
  ngOnInit(): void {
    this._CurrentUser = localStorage.getItem('_Currentuser');
    this._fullname = localStorage.getItem('UserfullName');
    this._EmpNo = sessionStorage.getItem('EmpNo');
  }

  logout() {
   
    this.loadingBar_state.stop();
    console.log('logout');
    this.authService.logout();
    // console.log(this.authService.logout()); 
    this.router.navigate(['']);
    // window.sessionStorage.clear();
    this.clearSession();
    //  window.localStorage.clear();
  }

  clearSession(): void {
    localStorage.clear();
    sessionStorage.clear();
  }
}
