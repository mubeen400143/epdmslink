import { Component, OnInit } from '@angular/core';
import { DropdownDTO } from 'src/app/_Models/dropdown-dto';
import { PortfolioDTO } from 'src/app/_Models/portfolio-dto';
import { SubTaskDTO } from 'src/app/_Models/sub-task-dto';
import { UserDetailsDTO } from 'src/app/_Models/user-details-dto';
import { ProjectTypeService } from 'src/app/_Services/project-type.service';
import { CompletedProjectsDTO } from 'src/app/_Models/completed-projects-dto';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-view-dashboard-projects',
  templateUrl: './view-dashboard-projects.component.html',
  styleUrls: ['./view-dashboard-projects.component.css']
})
export class ViewDashboardProjectsComponent implements OnInit {

  _ObjCompletedProj: CompletedProjectsDTO;
  btn_CreatePortfolio: boolean;
  hidetotalProjects: boolean;
  selectedItemsList: any;
  btnGetRecords: boolean;
  EmpCountInFilter = [];
  TypeContInFilter = [];
  StatusCountFilter = [];
  searchText: string;
  txtSearch: string;
  public _totalProjectsCount: number;

  portfolioName: string;
  submitted: boolean;
  public _ListProjStat: any[];
  countOwners: any;
  countAll: number;
  messageForEmpty: boolean;
  public _Pid: number;
  TotalProjects: any;
  public _ProjectsListBy_Pid: any;
  CountInprocess: any;
  CountDelay: any;
  search_Type: any[];
  search_status: any[];
  str_portName: string;
  StoredPortfolio_ID: number;

  //Declarations//
  master_checked: boolean = false;
  checked: boolean = false;
  CurrentPageNo: number = 1;
  _filtersMessage2: string;
  _filtersMessage: string;
  _ProjectDataList: any;
  ActualDataList: any;
  un_FilteredProjects: any = [];
  public _CurrentpageRecords: number;
  _StatusCountDB: any;
  //Properties
  ObjUserDetails: UserDetailsDTO;
  _objDropdownDTO: DropdownDTO;
  Obj_Portfolio_DTO: PortfolioDTO;
  portfolioId: number;
  Current_user_ID = sessionStorage.getItem('EmpNo');
  constructor(public service: ProjectTypeService,
    private loadingBar: LoadingBarService,
    private router: Router, private activatedRoute: ActivatedRoute) {
    this.ObjUserDetails = new UserDetailsDTO();
    this._objDropdownDTO = new DropdownDTO();
    this.Obj_Portfolio_DTO = new PortfolioDTO();
    this._ObjCompletedProj = new CompletedProjectsDTO();

  }
  Mode: string;
  _subtaskDiv: boolean;
  ngOnInit() {
    this.A2Z = true;
    this.Z2A = false;
    this._subtaskDiv = true;
    this.Mode = this.activatedRoute.snapshot.params.Mode;
    this.GetCompletedProjects();
    // this.notFoundData=true;
    //this.AssignedTask = true;
    //this.projectsDataTable = false;
    //this.portfolioName = localStorage.getItem('_PortfolioName');
  }
  Subtask_List: SubTaskDTO[];
  subtaskNotFoundMsg: string;
  _TotalSubtaskCount: number;
  pCode: string;
  pName: string;
  pDesc: string; pType: string; pStdt: Date; pEndDT: Date; pStat: string;
  pCost: number; pComp: string; pClient: string; Powner: string;
  PRespon: string; PAut: string; Pcoor: String; PInfo: String; pSupprt: string; pRType: string
  openInfo(pcode, pName, pDes, ptype, pStDt, pEnDT, pStat, pCost, pCom, pCli, pOwn, pRes, pAut, pCoor, pInf, pSup, pReportType) {

    this.pCode = pcode;
    this.pName = pName;
    this.pDesc = pDes;
    this.pType = ptype;
    this.pStdt = pStDt;
    this.pEndDT = pEnDT;
    this.pStat = pStat;
    this.pCost = pCost;

    this.pComp = pCom;
    this.pClient = pCli;
    this.Powner = pOwn;
    this.PRespon = pRes;
    this.PAut = pAut;
    this.Pcoor = pCoor;
    this.PInfo = pInf;
    this.pSupprt = pSup;
    this.pRType = pReportType;
    document.getElementById("mysideInfobar").style.width = "370px";

    this.service.SubTaskDetailsService(pcode).subscribe(
      (data) => {
        this.Subtask_List = data as SubTaskDTO[];
        //console.log("subtask Details 1---->", this.Subtask_List);
        if (this.Subtask_List.length == 0) {
          this._subtaskDiv = true;
          this.subtaskNotFoundMsg = "No Subtask found";
        }
        else {
          this._subtaskDiv = false;
          this.subtaskNotFoundMsg = "";
          this._TotalSubtaskCount = this.Subtask_List.length;
        }
      });
  }
  closeInfo() {
    document.getElementById("mysideInfobar").style.width = "0";
  }

  LoadingBar = this.loadingBar.useRef('http');
  projectsDataTable: boolean;
  AssignedTask: boolean;
  _AssignedProjectsList: any;
  _Statustitle: string;
  notFoundData: boolean;
  notSelectedAnything_msg: string;
  notSelectedAnything_msg2: string;
  GetCompletedProjects() {
    this.LoadingBar.start();
    // this.Mode = this.service._getMessage();
    let EmpNo = this.Current_user_ID;
    let Pgno: number = this.CurrentPageNo;
    //Passing to OBJ DTO...
    this._ObjCompletedProj.Mode = this.Mode;
    this._ObjCompletedProj.Emp_No = EmpNo;
    this._ObjCompletedProj.PageNumber = Pgno;

    if (this.Mode == "AssignedTask") {
      this.AssignedTask = false;
      this.projectsDataTable = true;
      this._Statustitle = "Assigned Projects";

      this.service._GetCompletedProjects(this._ObjCompletedProj).
        subscribe(data => {
          if (JSON.parse(data[0]['JsonData_Json']).length == 0) {
            this.notSelectedAnything_msg = "Sorry, No records found in " + this._Statustitle;
            this.notSelectedAnything_msg2 = "Please select from dashboard, the data you're looking for";
            this.LoadingBar.stop();
          }
          else {
            this._AssignedProjectsList = JSON.parse(data[0]['JsonData_Json']);
            this.LoadingBar.stop();
            //if (this._AssignedProjectsList.length > 0) {
            this._CurrentpageRecords = this._AssignedProjectsList.length;
            //this.service.Mode = "";
            // }
          }
        });
    }
    if (this.Mode != "AssignedTask" && this.Mode != "") {
      this.projectsDataTable = false;
      this.AssignedTask = true;

      if (this.Mode == "Delay") {
        this._Statustitle = "Delay Projects";
        this._totalProjectsCount = parseInt(sessionStorage.getItem('DelayCount'));
      }
      if (this.Mode == "UnderApproval") {
        this._Statustitle = "UnderApproval Projects";
        this._totalProjectsCount = parseInt(sessionStorage.getItem('CompletedCount'));
      }
      if (this.Mode == "Rejected") {
        this._Statustitle = "Rejected Projects";
        this._totalProjectsCount = parseInt(sessionStorage.getItem('RejectedCount'));
      }
      if (this.Mode == "ExOneMonth") {
        this._Statustitle = "Delay In One Month";
        this._totalProjectsCount = parseInt(sessionStorage.getItem('TotalExpiryInMonth'));
      }
      //Reset The List for New Data
      this._ProjectDataList = [];
      this.service._GetCompletedProjects(this._ObjCompletedProj)
        .subscribe((data) => {
          debugger
          // if (Object.entries(data).length == 1 ||Object.entries(data).length == 0) {
          //   this.notSelectedAnything_msg = "Sorry data not found !";
          //   this.notSelectedAnything_msg2 = "Please select from dashboard, the data your looking for";
          //   this.LoadingBar.stop();
          // }
          if (JSON.parse(data[0]['JsonData_Json']).length == 0) {
            this.notSelectedAnything_msg = "Sorry, No records found in " + this._Statustitle;
            this.notSelectedAnything_msg2 = "Please select from dashboard, the data you're looking for";
            this.LoadingBar.stop();
          }

          else {
            this._ProjectDataList = JSON.parse(data[0]['JsonData_Json']);
           
            if (this._ProjectDataList.length > 0) {
              this.LoadingBar.stop();
            }
            if (this.selectedItem_Emp.length == 0) {
              this.EmpCountInFilter = JSON.parse(data[0]['Employee_Json']);
             
            }
            else {
              this.EmpCountInFilter = this.selectedItem_Emp[0];
             
            }
            //Type
            if (this.selectedItem_Type.length == 0) {
              this.TypeContInFilter = JSON.parse(data[0]['ProjectType_Json']);
            }
            else {
              this.TypeContInFilter = this.selectedItem_Type[0];
            }
            //Status
            if (this.selectedItem_Status.length == 0) {
              this.StatusCountFilter = JSON.parse(data[0]['Status_Json']);
            }
            else {
              this.StatusCountFilter = this.selectedItem_Status[0];
            }

            // this.TypeContInFilter = JSON.parse(data[0]['ProjectType_Json']);
            // this.StatusCountFilter = JSON.parse(data[0]['Status_Json']);
            // this.EmpCountInFilter = JSON.parse(data[0]['Employee_Json']);


          }
          // else {
          //   this.LoadingBar.stop();
          //   this.notFoundData = false;
          // }
        });
    }
  }
  BackBttn() {
    this._ProjectDataList = [];
    this.service.Mode = "";
    this.LoadingBar.stop();
    this.router.navigate(['backend/Dashboard']);
  }
  selectedType_String: string;
  selectedEmp_String: string;
  selectedStatus_String: string;
  checkedItems_Status: any = [];
  checkedItems_Type: any = [];
  checkedItems_Emp: any = [];

  selectedItem_Status = [];
  isStatusChecked(item) {
    let arr = [];
    this.StatusCountFilter.forEach(element => {
      if (element.checked == true) {
        arr.push({ Status: element.Name });
        return this.checkedItems_Status = arr;
      }
    });
    let arr2 = [];
    this.StatusCountFilter.filter((item) => {
      if (item.checked == true) {
        this.applyFilters();
        return arr2.push(item);
      }
    });
    this.selectedItem_Status.push(arr2);
    this.StatusCountFilter.forEach(element => {
      if (element.checked == false) {
        this.selectedItem_Status.length = 0;
        this.resetFilters();
      }
    });
  }
  selectedItem_Type = [];
  isTypeChecked(item) {

    let arr = [];
    this.TypeContInFilter.forEach(element => {
      if (element.checked == true) {
        arr.push({ Block_No: element.Project_Block });
        return this.checkedItems_Type = arr;
      }
    });
    let arr2 = [];
    this.TypeContInFilter.filter((item) => {
      if (item.checked == true) {
        this.applyFilters();
        return arr2.push(item);
      }
    });
    this.selectedItem_Type.push(arr2);
    this.TypeContInFilter.forEach(element => {
      if (element.checked == false) {
        this.selectedItem_Type.length = 0;
        this.resetFilters();
      }
    });
  }
  selectedItem_Emp = [];
  isEmpChecked(item) {
    let arr = [];
    this.EmpCountInFilter.forEach(element => {
      if (element.checked == true) {
        arr.push({ Emp_No: element.Emp_No });
        return this.checkedItems_Emp = arr;
      }
    });
    let arr2 = [];
    this.EmpCountInFilter.filter((item) => {
      if (item.checked == true) {
        this.applyFilters();
        return arr2.push(item);
      }
    });
    this.selectedItem_Emp.push(arr2);
    this.EmpCountInFilter.forEach(element => {
      if (element.checked == false) {
        this.selectedItem_Emp.length = 0;
        this.resetFilters();
      }
    });
  }
  resetFilters() {

    this.searchText = "";
    this.search_Type = [];
    this.CurrentPageNo = 1;
    if (this.selectedItem_Type.length == 0) {
      this.selectedType_String = null;
      this.checkedItems_Type = [];
    }
    if (this.selectedItem_Status.length == 0) {
      this.selectedStatus_String = null;
      this.checkedItems_Status = [];
    }
    if (this.selectedItem_Emp.length == 0) {
      this.selectedEmp_String = null;
      this.checkedItems_Emp = [];
    }
    //console.log("On Reset--->", this.checkedItems_Type, this.checkedItems_Status, this.checkedItems_Emp);
    this.applyFilters();
  }
  //Apply Filters
  SearchbyText() {
    this.CurrentPageNo = 1;
    this.applyFilters();
  }
  applyFilters() {
    this.selectedEmp_String = this.checkedItems_Emp.map(select => {
      return select.Emp_No;
    }).join(',');

    this.selectedType_String = this.checkedItems_Type.map(select => {
      return select.Block_No;
    }).join(',');

    this.selectedStatus_String = this.checkedItems_Status.map(select => {
      return select.Status;
    }).join(',');

    //console.log(this.checkedItems_Status, this.checkedItems_Type, this.checkedItems_Emp);

    this._ObjCompletedProj.SelectedStatus = this.selectedStatus_String;
    this._ObjCompletedProj.SelectedEmp_No = this.selectedEmp_String;
    this._ObjCompletedProj.SelectedBlock_No = this.selectedType_String;

    this._ObjCompletedProj.PageNumber = this.CurrentPageNo;
    this._ObjCompletedProj.PageSize = 30;
    this._ObjCompletedProj.Project_SearchText = this.searchText;

    //console.log("string------->", this.selectedType_String, this.selectedEmp_String, this.selectedStatus_String);
    this.service._GetCompletedProjects(this._ObjCompletedProj)
      .subscribe(data => {

        //this.LoadingBar_state.start();
        //this._ProjectDataList = JSON.parse(data[0]['Projects_Json']);
        this._ProjectDataList = JSON.parse(data[0]['JsonData_Json']);
        this.TypeContInFilter = JSON.parse(data[0]['ProjectType_Json']);
        this.StatusCountFilter = JSON.parse(data[0]['Status_Json']);
        this.EmpCountInFilter = JSON.parse(data[0]['Employee_Json']);

        this._CurrentpageRecords = this._ProjectDataList.length;
        if (this._ProjectDataList) {
          //this.LoadingBar_state.stop();
        }
        if (this._ProjectDataList.length == 0) {
          this._filtersMessage = "No projects matched your search";
          this._filtersMessage2 = "Please try again";
        }
        else {
          this._filtersMessage = "";
          this._filtersMessage2 = "";
        }
      });
    //Filtering Checkbox de
  }
  resetAll() {
    this.txtSearch = '';
    this.selectedItem_Type.length = 0;
    this.selectedItem_Status.length = 0;
    this.selectedItem_Emp.length = 0
    this.resetFilters();
  }
  A2Z: boolean;
  Z2A: boolean;
  clicks: number = 0;
  _SortProjectList() {
    this.clicks += 1;
    if (this.clicks != 1) {
      this.A2Z = true;
      this.Z2A = false;
      this._ProjectDataList = this._ProjectDataList.sort((a, b) => (a.Project_Code > b.Project_Code) ? -1 : 1);
      this.clicks = 0;
    } else {
      this.A2Z = false;
      this.Z2A = true;
      this._ProjectDataList = this._ProjectDataList.sort((a, b) => (a.Project_Code > b.Project_Code) ? 1 : -1);
    }
  }
}
