import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { ConfirmDialogComponent } from 'src/app/Shared/components/confirm-dialog/confirm-dialog.component';
import { DropdownDTO } from 'src/app/_Models/dropdown-dto';
import { PortfolioDTO } from 'src/app/_Models/portfolio-dto';
import { StatusDTO } from 'src/app/_Models/status-dto';
import { SubTaskDTO } from 'src/app/_Models/sub-task-dto';
import { UserDetailsDTO } from 'src/app/_Models/user-details-dto';
import { NotificationService } from 'src/app/_Services/notification.service';
import { ProjectTypeService } from 'src/app/_Services/project-type.service';
import { Router } from '@angular/router';
import * as _ from 'underscore';
@Component({
  selector: 'app-projects-add',
  templateUrl: './projects-add.component.html',
  styleUrls: ['./projects-add.component.css']
})
export class ProjectsAddComponent implements OnInit {
  btn_CreatePortfolio: boolean;
  hidetotalProjects: boolean;
  selectedItemsList: any;
  btnGetRecords: boolean;
  EmpCountInFilter = [];
  TypeContInFilter = [];
  StatusCountFilter = [];
  searchText: string;
  txtSearch: string;
  public _totalProjectsCount: any;
  Portfolio_FormGroup: FormGroup;
  portfolioName: string;
  submitted: boolean;
  public _ListProjStat: any[];
  countOwners: any;
  countAll: number;
  messageForEmpty: boolean;
  public _Pid: number;
  TotalProjects: any;
  public _ProjectsListBy_Pid: any;
  CountInprocess: any;
  CountDelay: any;
  search_Type: any[];
  search_status: any[];
  str_portName: string;
  StoredPortfolio_ID: number;
  
  // textMessage:boolean=true;

  constructor(
    public service: ProjectTypeService,
    private loadingBar: LoadingBarService,
    private notifyService: NotificationService,
    private dialog: MatDialog,
    private router: Router

  ) {
    this.ObjUserDetails = new UserDetailsDTO();
    this._objDropdownDTO = new DropdownDTO();
    this.Obj_Portfolio_DTO = new PortfolioDTO();
  }

  //Properties
  ObjUserDetails: UserDetailsDTO;
  _objDropdownDTO: DropdownDTO;
  Obj_Portfolio_DTO: PortfolioDTO;

  //Declarations//
  master_checked: boolean = false;
  checked: boolean = false;
  CurrentPageNo: number = 1;
  _filtersMessage2: string;
  _filtersMessage: string;
  _ProjectDataList: any;
  ActualDataList: any;
  un_FilteredProjects: any = [];
  public _CurrentpageRecords: number;
  _StatusCountDB: any;
  LoadingBar_state = this.loadingBar.useRef('http');
  Current_user_ID = sessionStorage.getItem('EmpNo');
  _portfolioId:any;

  subtaskDiv:boolean;
  ngOnInit() {
    this.portfolioName =  localStorage.getItem('_PortfolioName');
    this._portfolioId=sessionStorage.getItem('Pid');
    // this._portfolioId=localStorage.getItem('_PortfolioId');
    this.GetProjectsByUserName();
    this.getDropdownsDataFromDB();
  }
  GetProjectsByUserName() {
    this.LoadingBar_state.start();
    this.ObjUserDetails.PageNumber = this.CurrentPageNo;
    this.ObjUserDetails.PageSize = 30;
    this.ObjUserDetails.PortfolioId= parseInt(this._portfolioId,10);
   // console.log(this.ObjUserDetails.PortfolioId);
    this.service.GetProjectsByUserName_Service(this.ObjUserDetails).subscribe(data => {
     
      this._ProjectDataList = data;
      this.ActualDataList = data;
      this.un_FilteredProjects = this.ActualDataList;
      if (this._ProjectDataList) {
        this.LoadingBar_state.stop();
      }
      if (this._ProjectDataList) {
        this._CurrentpageRecords = this._ProjectDataList.length;
        //console.log("ProjectList----------->", this._ProjectDataList.length);
        ///console.log("ProjectList----------->", this._ProjectDataList);
      }
    });
  }

  getDropdownsDataFromDB() {
    this._objDropdownDTO.EmpNo = this.Current_user_ID;
    this._objDropdownDTO.Selected_ProjectType = this.selectedType_String;
    this._objDropdownDTO.Selected_Status = this.selectedStatus_String;
    this._objDropdownDTO.SelectedEmp_No = this.selectedEmp_String;
    this._objDropdownDTO.Selected_SearchText = this.searchText;
    //debugger
    this._objDropdownDTO.PortfolioId=parseInt(this._portfolioId,10);
    this.service.GetDropDownsData(this._objDropdownDTO)
      .subscribe((data) => {
        //Emp
        if (this.selectedItem_Emp.length == 0) {
          this.EmpCountInFilter = JSON.parse(data[0]['Emp_Json']);
        }
        else {
          this.EmpCountInFilter = this.selectedItem_Emp[0];
        }
        //Type
        if (this.selectedItem_Type.length == 0) {
          this.TypeContInFilter = JSON.parse(data[0]['ProjectType_Json']);
        }
        else {
          this.TypeContInFilter = this.selectedItem_Type[0];
        }
        //Status
        if (this.selectedItem_Status.length == 0) {
          this.StatusCountFilter = JSON.parse(data[0]['Status_Json']);
        }
        else {
          this.StatusCountFilter = this.selectedItem_Status[0];
        }
        this._totalProjectsCount = JSON.parse(data[0]['TotalProjectsCount_Json']);
        this._totalProjectsCount = this._totalProjectsCount[0]['TotalProjects'];
        //console.log(this._totalProjectsCount)
      });
  }
  master_CheckBox() {
    for (let value of Object.values(this._ProjectDataList)) {
      value['checked'] = this.master_checked;
      if (value['checked'] === true) {
        this.btn_CreatePortfolio = false;
        this.hidetotalProjects = true;
      }
      else {
        this.btn_CreatePortfolio = true;
        this.hidetotalProjects = false;
      }
      this.selectedItemsList = this._ProjectDataList.filter((checkboxes) => {
        return checkboxes.checked == true;
      });
      //console.log("selectedList------>", this.selectedItemsList);
    }
  }
  checkboxclick(item, Pcode) {

    if (item == true) {
      for (let value of Object.values(this._ProjectDataList)) {
        if (value['checked'] == true && value['Project_Code'] == Pcode) {
          this.OnSave();
          return true;
        }
      }
    }
    if (item == false) {
      console.log(this._ProjectDataList)
      this._ProjectDataList.forEach(element => {
        if (element.checked == false && element.Project_Code == Pcode) {
          let prid = element.id;
          let poid = this._portfolioId;
          let Projname = element.Project_Name;
          let pCode=element.Project_Code;
          let Createdby=element.Emp_No;
          this.DeleteProject(prid, poid, pCode,Projname,Createdby);
          return true;
        }
      });
    }
  }
  fetchSelectedItems() {
    this.selectedItemsList = this._ProjectDataList.filter((checkboxes) => {
      return checkboxes.checked == true;
    });
  }
  dataTableCheckbox() {
    this.btnGetRecords = true;
    this.fetchSelectedItems();
    this.str_portName = this.portfolioName;
  }
  checkedItems_Status: any = [];
  checkedItems_Type: any = [];
  checkedItems_Emp: any = [];
  selectedType_String: string;
  selectedEmp_String: string;
  selectedStatus_String: string;

  selectedItem_Status = [];
  isStatusChecked(item) {
    let arr = [];
    this.StatusCountFilter.forEach(element => {
      if (element.checked == true) {
        arr.push({ Status: element.Name });
        return this.checkedItems_Status = arr;
      }
    });
    let arr2 = [];
    this.StatusCountFilter.filter((item) => {
      if (item.checked == true) {
        this.applyFilters();
        return arr2.push(item);
      }
    });
    this.selectedItem_Status.push(arr2);
    this.StatusCountFilter.forEach(element => {
      if (element.checked == false) {
        this.selectedItem_Status.length = 0;
        this.resetFilters();
      }
    });

  }
  selectedItem_Type = [];
  isTypeChecked(item) {

    let arr = [];
    this.TypeContInFilter.forEach(element => {
      if (element.checked == true) {
        arr.push({ Block_No: element.Block_No });
        return this.checkedItems_Type = arr;
      }
    });
    let arr2 = [];
    this.TypeContInFilter.filter((item) => {
      if (item.checked == true) {
        this.applyFilters();
        return arr2.push(item);
      }
    });
    this.selectedItem_Type.push(arr2);
    this.TypeContInFilter.forEach(element => {
      if (element.checked == false) {
        this.selectedItem_Type.length = 0;
        this.resetFilters();
      }
    });

  }
  selectedItem_Emp = [];
  isEmpChecked(item) {
    let arr = [];
    this.EmpCountInFilter.forEach(element => {
      if (element.checked == true) {
        arr.push({ Emp_No: element.Emp_No });
        return this.checkedItems_Emp = arr;
      }
    });
    let arr2 = [];
    this.EmpCountInFilter.filter((item) => {
      if (item.checked == true) {
        this.applyFilters();
        return arr2.push(item);
      }
    });
    this.selectedItem_Emp.push(arr2);
    this.EmpCountInFilter.forEach(element => {
      if (element.checked == false) {
        this.selectedItem_Emp.length = 0;
        this.resetFilters();
      }
    });

  }
  //Apply Filters
  SearchbyText() {
    this.CurrentPageNo = 1;
    this.applyFilters();
  }
  applyFilters() {
    this.selectedEmp_String = this.checkedItems_Emp.map(select => {
      return select.Emp_No;
    }).join(',');

    this.selectedType_String = this.checkedItems_Type.map(select => {
      return select.Block_No;
    }).join(',');

    this.selectedStatus_String = this.checkedItems_Status.map(select => {
      return select.Status;
    }).join(',');

    //console.log(this.checkedItems_Status, this.checkedItems_Type, this.checkedItems_Emp);

    this.ObjUserDetails.SelectedStatus = this.selectedStatus_String;
    this.ObjUserDetails.SelectedEmp_No = this.selectedEmp_String;
    this.ObjUserDetails.SelectedBlock_No = this.selectedType_String;

    this.ObjUserDetails.PageNumber = this.CurrentPageNo;
    this.ObjUserDetails.PageSize = 30;
    this.ObjUserDetails.SearchText = this.searchText;
    this.ObjUserDetails.PortfolioId = parseInt(this._portfolioId);
    //console.log("string------->", this.selectedType_String, this.selectedEmp_String, this.selectedStatus_String);
    this.service.GetProjectsByUserName_Service(this.ObjUserDetails)
      .subscribe(data => {
        this.LoadingBar_state.start();
        //this._ProjectDataList = JSON.parse(data[0]['Projects_Json']);
        this._ProjectDataList = data;
        this._CurrentpageRecords = this._ProjectDataList.length;
        if (this._ProjectDataList) {
          this.LoadingBar_state.stop();
        }
        if (this._ProjectDataList.length == 0) {
          this._filtersMessage = "No more projects matched your search";
          this._filtersMessage2 = " Clear the filters & try again";
          if (this.LoadingBar_state) {
            this.LoadingBar_state.stop();
          }
        }
        else {
          this._filtersMessage = "";
          this._filtersMessage2 = "";
        }
      });

    //Filtering Checkbox de
    this.getDropdownsDataFromDB();
  }

  Subtask_List: SubTaskDTO[];
  subtaskNotFoundMsg: string;
  _TotalSubtaskCount: number;
  pCode: string;
  pName: string;
  pDesc: string; pType: string; pStdt: Date; pEndDT: Date; pStat: string;
  pCost: number; pComp: string; pClient: string; Powner: string;
  PRespon: string; PAut: string; Pcoor: String; PInfo: String; pSupprt: string; pRType: string
  openInfo(pcode, pName, pDes, ptype, pStDt, pEnDT, pStat, pCost, pCom, pCli, pOwn, pRes, pAut, pCoor, pInf,pSup, pReportType) {
  
    this.pCode = pcode;
    this.pName = pName;
    this.pDesc = pDes;
    this.pType = ptype;
    this.pStdt = pStDt;
    this.pEndDT = pEnDT;
    this.pStat = pStat;
    this.pCost = pCost;

    this.pComp = pCom;
    this.pClient = pCli;
    this.Powner = pOwn;
    this.PRespon = pRes;
    this.PAut = pAut;
    this.Pcoor = pCoor;
    this.PInfo = pInf;
    this.pSupprt = pSup;
    this.pRType = pReportType;
    document.getElementById("mysideInfobar").style.width = "410px";
    this.service.SubTaskDetailsService(pcode).subscribe(
      (data) => {
        this.Subtask_List = data as SubTaskDTO[];
        //console.log("subtask Details 1---->", this.Subtask_List);
        if(this.Subtask_List.length==0){
          this.subtaskDiv=true;
          this.subtaskNotFoundMsg="No Subtask found";
        }
        else{
          this.subtaskDiv=false;
          this.subtaskNotFoundMsg="";
          this._TotalSubtaskCount = this.Subtask_List.length;
        }
      });
  }
  closeInfo() {
    document.getElementById("mysideInfobar").style.width = "0";
  }
  //Save Portfolio
  OnSave() {
    this.Obj_Portfolio_DTO.Portfolio_Name = this.portfolioName;
    let LengthOfSelectedItems: any;
    LengthOfSelectedItems = JSON.stringify(this.selectedItemsList.length);
    this.Obj_Portfolio_DTO.SelectedProjects = this.selectedItemsList;
    this.service.SavePortfolio(this.Obj_Portfolio_DTO)
      .subscribe(data => {
        this._portfolioId = data['Portfolio_ID'];
        //console.log("Return value--------->", this._portfolioId);
        this.service.GetPortfolioStatus(this.Current_user_ID).subscribe(
          (data) => {
            this._ListProjStat = data as StatusDTO[];
            //console.log("ListForStatus", this._ListProjStat);
            //Owners Portfolios
            let Listown: any = this._ListProjStat.filter(i => (i.CreatedName));
            this.countOwners = Listown.length;
            this.countAll = this._ListProjStat.length;
            if (this._ListProjStat.length == 0) {
              this.messageForEmpty = false;//"No Portfolio's has created";
            }
            else {
              this.messageForEmpty = true;
            }
          });
      });

     //debugger
    if (this._portfolioId == 0 ||this._portfolioId==null||this._portfolioId=='') {
      this.notifyService.showSuccess("Portfolio Created " + ' ' + ' Added ' + ' ' + LengthOfSelectedItems + ' ' + 'Project(s)', '');
    }
    if (this._portfolioId!='') {
      this.notifyService.showInfo("" + ' ' + 'Added' + ' ' + LengthOfSelectedItems + ' ' + 'Project(s)', '');
    }
  }

  //Function Reset Filters
  resetFilters() {
    this.searchText = "";
    this.search_Type = [];
    this.CurrentPageNo = 1;

    if (this.selectedItem_Type.length == 0) {
      this.selectedType_String = null;
      this.checkedItems_Type = [];
    }
    if (this.selectedItem_Status.length == 0) {
      this.selectedStatus_String = null;
      this.checkedItems_Status = [];
    }
    if (this.selectedItem_Emp.length == 0) {
      this.selectedEmp_String = null;
      this.checkedItems_Emp = [];
    }
    //console.log("On Reset--->", this.checkedItems_Type, this.checkedItems_Status, this.checkedItems_Emp);
    this.applyFilters();
  }
  resetAll(){
    this.txtSearch='';
    this.selectedItem_Type.length=0;
    this.selectedItem_Status.length=0;
    this.selectedItem_Emp.length=0
    this.resetFilters();
  }
  BackBttn(){
    this.router.navigate(['backend/Portfolio'])
    //this.router.serializeUrl(this.router.createUrlTree(['Home/Portfolio/']));
    // const Url = this.router.serializeUrl(this.router.createUrlTree(['Home/Portfolio/']));
    // window.open(Url);
  }
  deletedBy:string;
  DeleteProject(Proj_id:number, port_id:number,Pcode:string,proj_Name:string, createdBy:string) {
    this.deletedBy=this.Current_user_ID;
    let String_Text = 'Delete';
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        mode: 'delete',
        title1: 'Confirmation ',
        message1: proj_Name
      }
    });
    confirmDialog.afterClosed().subscribe(result => {
      if (result === true) {
        this.service.DeleteProject(Proj_id,port_id,Pcode,proj_Name,createdBy,this.deletedBy).subscribe((data) => {
          this.service.GetProjectsBy_portfolioId(this._Pid)
            .subscribe((data) => {
              //console.log("Retrun After Delete :" + data);
              this._ProjectsListBy_Pid = JSON.parse(data[0]['JosnProjectsByPid']);
              this._StatusCountDB = JSON.parse(data[0]['JsonStatusCount']);
              this.notifyService.showSuccess("Project removed successfully ", '');
            });
          this.service.GetPortfolioStatus(this.Current_user_ID).subscribe(
            (data) => {
              this._ListProjStat = data as StatusDTO[];
            });
        })
      }
      else {
        //debugger
        this.notifyService.showInfo("Action Cancelled ", '');
        //checking again if delete Cancel
        this._ProjectDataList.forEach(element => {
          if (element.checked==false && element.id == Proj_id) {
           return element.checked = true;

          }
        });
      }
    });
  }

}
