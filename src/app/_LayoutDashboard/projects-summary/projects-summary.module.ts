import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ProjectsSummaryRoutingModule } from './projects-summary-routing.module';
import { ProjectsSummaryComponent } from './projects-summary.component';

import { Ng2SearchPipeModule } from 'ng2-search-filter';
// import { NgSelectModule } from '@ng-select/ng-select';
// import { NgxPaginationModule } from 'ngx-pagination';
//import { ToastrModule } from 'ngx-toastr';
import { MaterialModule } from "src/app/material-module";
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
//import { SortDirective } from 'src/app/Directive/sort.directive';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { DateAgoPipe } from 'src/app/Shared/DateAgo/date-ago.pipe';

@NgModule({
  declarations: [
    ProjectsSummaryComponent
    ,DateAgoPipe
  //  SortDirective
  ],
  imports: [
    CommonModule,
    ProjectsSummaryRoutingModule,
    FormsModule,
    MaterialModule,
    Ng2SearchPipeModule,
    AutocompleteLibModule,
    NgbModule,
    NgMultiSelectDropDownModule
    
  ]
})
export class ProjectsSummaryModule { }
