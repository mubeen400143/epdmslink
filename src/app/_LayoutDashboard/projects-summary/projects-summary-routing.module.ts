import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProjectsSummaryComponent } from './projects-summary.component';

const routes: Routes = [{ path: '', component: ProjectsSummaryComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectsSummaryRoutingModule { }
