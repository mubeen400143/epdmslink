import { Component, OnInit } from '@angular/core';
import { NotificationActivityDTO } from 'src/app/_Models/notification-activity-dto';
import { StatusDTO } from 'src/app/_Models/status-dto';
//import { ScriptService } from 'src/app/_Services/script.service';
import { ProjectTypeService } from 'src/app/_Services/project-type.service';
import { CalendarOptions } from '@fullcalendar/angular';
// import { ConfirmDialogComponent } from 'src/app/Shared/components/confirm-dialog/confirm-dialog.component';
// import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { CompletedProjectsDTO } from 'src/app/_Models/completed-projects-dto';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { SubTaskDTO } from 'src/app/_Models/sub-task-dto';

//import { data } from 'jquery';
//import { DatePipe } from '@angular/common';
//import dayGridPlugin from '@fullcalendar/daygrid'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  posts = [];
  calendarOptions: CalendarOptions;

  _NotificationActivityList: NotificationActivityDTO[];
  _RequestActivity: [];
  _DarActivityList: [];
  Emp_No: string;
  _Notification: any;
  projectactivity_Div: boolean;
  DARactivity_Div: boolean;
  _subtaskDiv: boolean;

  //Portfolio Variables.
  _ListProjStat: StatusDTO[];
  Current_user_ID: any;
  messageForEmpty: boolean;
  myDate: number;
  CurrentUser_fullname: string;
  _ObjCompletedProj: CompletedProjectsDTO;
  _ActualProjectList = [];
  _CalendarProjectsList = {};


  constructor(public service: ProjectTypeService,
    private loadingBar: LoadingBarService,
    private router: Router
  ) {

    this._ObjCompletedProj = new CompletedProjectsDTO();
  }
  Subtask_List: SubTaskDTO[];
  subtaskNotFoundMsg: string;
  _TotalSubtaskCount: number;
  pCode: string;
  StartDate: string; EndDate: string; ProjectName: string;
  ProjectCode: string; Status: string; ProjectType: string; Owner: string;
  Responsible: string; Autho: string; Coordinator: string; Informer: string;
  Support: string; Description: string; Com_Name: string; Project_Cost: number; Client_Name: string
  handleEventClick(arg) {
    this.StartDate = arg.event.start,
      this.EndDate = arg.event.end,
      this.ProjectName = arg.event.title,
      this.ProjectCode = arg.event._def.extendedProps.Project_Code;
    this.Status = arg.event._def.extendedProps.Status;
    this.ProjectType = arg.event._def.extendedProps.ProjectType;
    this.Owner = arg.event._def.extendedProps.ProjectOwner;
    this.Responsible = arg.event._def.extendedProps.TeamRes;
    this.Autho = arg.event._def.extendedProps.TeamAutho;
    this.Coordinator = arg.event._def.extendedProps.TeamCoor;
    this.Informer = arg.event._def.extendedProps.TeamInformer;
    this.Support = arg.event._def.extendedProps.TeamSupport;

    this.Description = arg.event._def.extendedProps.Project_Description;
    this.Com_Name = arg.event._def.extendedProps.Com_Name;
    this.Project_Cost = arg.event._def.extendedProps.Project_Cost;
    this.Client_Name = arg.event._def.extendedProps.Client_Name;
    this.pCode = arg.event._def.extendedProps.Project_Code;

    document.getElementById("mysideInfobar").style.width = "370px";


    this.service.SubTaskDetailsService(this.pCode).subscribe(
      (data) => {
        this.Subtask_List = data as SubTaskDTO[];

        if (this.Subtask_List.length == 0) {
          this._subtaskDiv = true;
          this.subtaskNotFoundMsg = "No Subtask found";
        }
        else {
          this._subtaskDiv = false;
          this.subtaskNotFoundMsg = "";
          this._TotalSubtaskCount = this.Subtask_List.length;
        }
      });
  }
  closeInfo() {
    document.getElementById("mysideInfobar").style.width = "0";
  }

  ngOnInit() {
    this._subtaskDiv=true;
    this.projectactivity_Div = false;
    this.DARactivity_Div = true;
    this.Current_user_ID = sessionStorage.getItem('EmpNo');
    this.CurrentUser_fullname = localStorage.getItem('UserfullName');
    this.GetDashboardSummary();
    this.service.GetActivities(this.Current_user_ID).subscribe(
      (data) => {
        this._NotificationActivityList = data as NotificationActivityDTO[];
        this._RequestActivity = JSON.parse(this._NotificationActivityList[0]['RequestActivity_Json']);
        this._DarActivityList = JSON.parse(this._NotificationActivityList[0]['DarActivity_Json']);
      });
    this.service.GetPortfolioStatus(this.Current_user_ID).subscribe(
      (data) => {
        this._ListProjStat = data as StatusDTO[];
        var counts = {};
        this._ListProjStat.forEach(function (x) { counts[x.Status] = (counts[x.Status] || 0) + 1; });
        this.TotalExpiryPortfolio = counts['Delay'];
        if (this._ListProjStat.length == 0) {
          this.messageForEmpty = false;
        }
        else {
          this.messageForEmpty = true;
        }
      });
    //Dashboard_Summary
    this.GetCalendarProjects();
    this.LoadingBar.stop();
  }
  GetCalendarProjects() {
    let Empno: string = this.Current_user_ID;
    this.service._GetCalendarProjects(Empno).subscribe
      ((data) => {
        this._ActualProjectList = data as CompletedProjectsDTO[];
        //this._ActualProjectList = data;
        this._CalendarProjectsList = this._ActualProjectList.sort((a, b) => (a.ProjectType > b.ProjectType) ? 1 : -1);
        //Calendar Options
        this.calendarOptions = {
          initialView: 'dayGridMonth',
          headerToolbar: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,dayGridWeek,dayGridDay,listWeek'
          },
          //height: '100%',
          //displayEventTime: true,
          //dayMaxEvents: true,
          //dateClick: this.handleDateClick.bind(this),

          eventClick: this.handleEventClick.bind(this),
          events: this._CalendarProjectsList,
          //eventDisplay:'auto',
          // displayEventEnd:false,
          // eventOverlap:true,
          dayMaxEvents: 6,



          // eventColor: '#f9d57f',
          // eventTextColor: '#323232',
          // displayEventEnd: false,
          //  lazyFetching: true,
          //showMore = true,

          // nowIndicator: true,
        };
      });
  }

  //GetSummaryCounts
  DelayCount: any = sessionStorage.getItem('DelayCount');
  CompletedCount: any = sessionStorage.getItem('CompletedCount');
  TotalExpiryInMonth: any = sessionStorage.getItem('TotalExpiryInMonth');
  TotalExpiryPortfolio: number;
  EmployeeVacationInDays: any = sessionStorage.getItem('EmployeeVacationInDays');
  TotalDARSubmitted: any = sessionStorage.getItem('TotalDARSubmitted');
  TodaysDARAchievement: any = sessionStorage.getItem('TodaysDARAchievement');
  TotalDARRejected: any = sessionStorage.getItem('TotalDARRejected');
  YesterdaysDAR_Status: any = sessionStorage.getItem('YesterdaysDAR_Status');
  RejectedCount: any = sessionStorage.getItem('RejectedCount');
  AssignedProjects: any = sessionStorage.getItem('AssignedProjects');

  ProjectsNotStarted: any = sessionStorage.getItem('ProjectsNotStarted');
  ProjectsNotWorking: any = sessionStorage.getItem('ProjectsNotWorking');

  _ProjectDataList: any;
  LoadingBar = this.loadingBar.useRef('http');
  _dashboardData: any;
  GetDashboardSummary() {
    this.LoadingBar.start();
    this.Emp_No = sessionStorage.getItem('EmpNo');
    this.service._GetDashboardSummaryCount(this.Emp_No)
      .subscribe((data) => {
        if (data) {
          this.LoadingBar.stop();
        }
        this.DelayCount = data[0]['DelayCount'];
        sessionStorage.setItem('DelayCount', this.DelayCount);

        this.CompletedCount = data[0]['CompletedCount'];
        sessionStorage.setItem('CompletedCount', this.CompletedCount);

        this.TotalExpiryInMonth = data[0]['ExpiryOneMonth'];
        sessionStorage.setItem('TotalExpiryInMonth', this.TotalExpiryInMonth);

        this.EmployeeVacationInDays = data[0]['EmployeeVacationInDays'];
        sessionStorage.setItem('EmployeeVacationInDays', this.EmployeeVacationInDays);

        this.TotalDARSubmitted = data[0]['TotalDARSubmitted'];
        sessionStorage.setItem('TotalDARSubmitted', this.TotalDARSubmitted);

        this.TodaysDARAchievement = data[0]['TodaysDARAchievement'];
        sessionStorage.setItem('TodaysDARAchievement', this.TodaysDARAchievement);

        this.TotalDARRejected = data[0]['TotalDARRejected'];
        sessionStorage.setItem('TotalDARRejected', this.TotalDARRejected);

        this.YesterdaysDAR_Status = data[0]['YesterdaysDAR_Status'];
        sessionStorage.setItem('YesterdaysDAR_Status', this.YesterdaysDAR_Status);

        this.RejectedCount = data[0]['RejectedCount'];
        sessionStorage.setItem('RejectedCount', this.RejectedCount);

        this.AssignedProjects = data[0]['AssignedProjects'];
        sessionStorage.setItem('AssignedProjects', this.AssignedProjects);

        this.ProjectsNotStarted = data[0]['ProjectsNotStarted'];
        sessionStorage.setItem('ProjectsNotStarted', this.ProjectsNotStarted);

        this.ProjectsNotWorking = data[0]['ProjectsNotWorking'];
        sessionStorage.setItem('ProjectsNotWorking', this.ProjectsNotWorking);
      });
  }

  OnClickProjAct() {
    this.projectactivity_Div = false;
    this.DARactivity_Div = true;
  }
  OnClickDarAct() {
    this.projectactivity_Div = true;
    this.DARactivity_Div = false;
  }
  UnderApproval_Click() {
    this._ProjectDataList = [];
    let Mode: string = "UnderApproval";
    //this.service._setMessage(Mode);
    this.router.navigate(['backend/ViewProjects/', Mode]);
    //const Url = this.router.serializeUrl(this.router.createUrlTree(['Home/ViewProjects/', Mode]));
    //window.open(Url);

  }
  Delay_Click() {
    //alert("ok")
    this._ProjectDataList = [];
    let Mode: string = "Delay";
    this.router.navigate(['backend/ViewProjects/', Mode]);
    //const Url = this.router.serializeUrl(this.router.createUrlTree(['Home/ViewProjects/', Mode]));
    //window.open(Url);

  }
  Rejected_Click() {
    this._ProjectDataList = [];
    let Mode: string = "Rejected";
    //this.service._setMessage(Mode);
    this.router.navigate(['backend/ViewProjects/', Mode]);
    // const Url = this.router.serializeUrl(this.router.createUrlTree(['testcreativeplanner/Home/ViewProjects/', Mode]));
    // window.open(Url);

  }
  ExpiryInOneMonth_Click() {
    this._ProjectDataList = [];
    let Mode: string = "ExOneMonth";
    //this.service._setMessage(Mode);
    this.router.navigate(['backend/ViewProjects/', Mode]);
    // const Url = this.router.serializeUrl(this.router.createUrlTree(['testcreativeplanner/Home/ViewProjects/', Mode]));
    // window.open(Url);

  }
  _AssignedProjectsList: any;
  AssignedTask_Click() {
    let Mode: string = "AssignedTask";
    // this.service._setMessage(Mode);
    this.router.navigate(['backend/ViewProjects/', Mode]);
    // const Url = this.router.serializeUrl(this.router.createUrlTree(['testcreativeplanner/Home/ViewProjects/', Mode]));
    // window.open(Url);

  }
  Portfolio_Click() {
    //this.router.navigate(['/Portfolios/']);
    this.router.navigate(['backend/Portfolio/']);
    // const Url = this.router.serializeUrl(this.router.createUrlTree(['testcreativeplanner/Home/Portfolio/']));
    // window.open(Url);
  }
}

