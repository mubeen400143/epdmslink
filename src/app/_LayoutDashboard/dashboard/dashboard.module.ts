import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';

import { FullCalendarModule } from '@fullcalendar/angular';

import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import listPlugin from '@fullcalendar/list';
//import { SidebarComponent } from "src/app/_LayoutDashboard/sidebar/sidebar.component";
//import { DefaultComponent } from 'src/app/_Layouts/default/default.component';
//import { SidebarComponent } from 'src/app/Shared/components/sidebar/sidebar.component';


FullCalendarModule.registerPlugins([
  dayGridPlugin,
  interactionPlugin,
  listPlugin,
]);

@NgModule({
  declarations: [
    DashboardComponent,
   
   // DefaultComponent,
    //SidebarComponent
    //SidebarComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FullCalendarModule
  ],
  exports:[DashboardComponent]
})
export class DashboardModule { }
