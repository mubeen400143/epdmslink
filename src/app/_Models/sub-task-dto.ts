export class SubTaskDTO {
  Project_Code:string;
  SubProject_Name :string;
  StartDate :Date;
  SubProject_DeadLine:Date;
  SubProject_Status :string;
  Project_Cost:number;
}