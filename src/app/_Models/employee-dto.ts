export class EmployeeDTO {
    Emp_No:string;
    TM_DisplayName:string;
    Emp_First_Name:string;
    Emp_Second_Name:string;
    Emp_Last_Name:string;
    Position:string;
    Emp_Comp_No:string;
    Emp_Dept_No:string;
    Portfolio_ID:number;
    checked:boolean;
}
