export class ColumnChartDTO {
    Emp_No :string;
    TypeCount:number;
    ProjectType:string;
    Completed_Count:string;
    TotalProjects_Count:number;
    TotalComplete_Count:number;
    TotalProject:string;
}
