import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { BackendLayoutComponent } from './_Layouts/backend-layout/backend-layout.component';
import {BACKEND_ROUTES  } from "./routes/backend-layout-route";
const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./_LayoutDefault/login/login.module').then(m => m.LoginModule)
  },
  {
    path:'backend',component:BackendLayoutComponent,children:BACKEND_ROUTES,
    canActivate:[AuthGuard]
  },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
