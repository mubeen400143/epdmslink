import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { LinkDTO } from '../_Models/link-dto';
import { ApiurlService } from './apiurl.service';

@Injectable({
  providedIn: 'root'
})
export class LinkService {
  ObjLinkDTO: LinkDTO;

  readonly DMS_URL = "https://cswebapps.com/dmsapi/api/";

  constructor(private http: HttpClient, private commonUrl: ApiurlService) {
    this.ObjLinkDTO = new LinkDTO();
  }
  //readonly DMS_URL = sessionStorage.getItem("_AppUrl");
  //EP Common URL
  readonly rootUrl = this.commonUrl.apiurl;

  GetApplicationDetails() {
    return this.http.post(this.rootUrl + "ApplicationLinkAPI/NewGetApplication_Details", this.ObjLinkDTO);
  }

  GetMemosByEmployeeCode(CurrentUser) {
    this.ObjLinkDTO.EmployeeCode = CurrentUser;
    return this.http.post(this.DMS_URL + "/LatestCommunicationAPI/NewGetMemosByEmployeeCode", this.ObjLinkDTO);
  }

  InsertMemosOn_ProjectCode(projCode, appId, memoId, UserId) {
    // console.log("In Service ---->",projCode,appId,memoId,UserId);
    this.ObjLinkDTO.Project_Code = projCode;
    this.ObjLinkDTO.Application_Id = appId;
    this.ObjLinkDTO.JsonData = memoId;
    this.ObjLinkDTO.Created_By = UserId;
    return this.http.post(this.rootUrl + "ApplicationLinkAPI/NewInsertMemobyProjectCode", this.ObjLinkDTO);
  }

  _GetOnlyMemoIdsByProjectCode(projectCode) {
    this.ObjLinkDTO.Project_Code = projectCode;
    return this.http.post(this.rootUrl + "ApplicationLinkAPI/NewGetOnlyMemoIdsByProjectCode", this.ObjLinkDTO);
  }
  _GetMemosSubject(JsonString) {
    console.log("Sending JsonFormat----->",JsonString);
   
    this.ObjLinkDTO.MemosJSON = JsonString;
    return this.http.post(this.DMS_URL + "/LatestCommunicationAPI/NewGetMemosSubject", this.ObjLinkDTO);
  }
}
